plugins {
	kotlin("kapt")
	id("org.springframework.boot")
	kotlin("jvm")
	kotlin("plugin.spring")
	id("com.google.cloud.tools.jib")
	id("io.spring.dependency-management")
}

group = "cloud.dmytrominochkin"
version = "0.0.1-SNAPSHOT"

java.sourceCompatibility = JavaVersion.VERSION_17

extra["springCloudVersion"] = "2022.0.4"

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
	}
}
dependencies {
	kapt("org.springframework.boot:spring-boot-configuration-processor")

	// common
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("io.micrometer:micrometer-registry-prometheus")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("io.micrometer:micrometer-tracing-bridge-brave")
	implementation("io.zipkin.reporter2:zipkin-reporter-brave")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
}

jib {
	to {
		image = "gcr.io/${System.getenv("GOOGLE_CLOUD_PROJECT")}/message"
		tags = setOf("v1", "latest")
	}
	container {
		ports = listOf("8080")
		labels.putAll(
			mapOf(
				"maintainer" to "Dmytro Minochkin <dmytro.minochkin@gmail.com>",
				"org.opencontainers.image.title" to "message",
				"org.opencontainers.image.description" to "An example Message service",
				"org.opencontainers.image.version" to "$version",
				"org.opencontainers.image.authors" to "Dmytro Minochkin <dmytro.minochkin@gmail.com>",
				"org.opencontainers.image.url" to "https://gitlab.com/DmyMi/gcp-k8s-lab"
			)
		)
	}
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "17"
	}
}