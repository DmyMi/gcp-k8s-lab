package cloud.dmytrominochkin.message

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("demo")
data class DemoProperties(
        val greeting: String,
        val version: String = "1.0"
)