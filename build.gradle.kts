plugins {
    id("org.springframework.boot") version "3.1.6"
    kotlin("jvm") version "1.8.22" apply false
    kotlin("plugin.spring") version "1.8.22" apply false
    id("com.google.cloud.tools.jib") version "3.4.0" apply false
    id("io.spring.dependency-management") version "1.1.4" apply false
}

buildscript {
    repositories {
        mavenCentral()
        maven {
            url = uri("https://plugins.gradle.org/m2/")
        }
    }
}

allprojects {
    repositories {
        mavenCentral()
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
