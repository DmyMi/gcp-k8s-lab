package cloud.dmytrominochkin.guestbook

import java.io.Serializable
import java.util.*
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id

@Entity
data class Message(
        @Id
        @GeneratedValue
        var id: Long,
        var username: String,
        var message: String,
        var timestamp: Date
) : Serializable