package cloud.dmytrominochkin.guestbook

import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface MessageService : PagingAndSortingRepository<Message, Long>, CrudRepository<Message, Long> {
}